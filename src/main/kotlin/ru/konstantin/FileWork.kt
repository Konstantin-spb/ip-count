package ru.konstantin

import kotlinx.coroutines.*
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import kotlin.io.path.Path
import kotlin.math.pow
import kotlin.streams.toList

class FileWork {

    lateinit var testDir: Path

    suspend fun getUniqIPs(path: String, countOfPart: Int) {
        testDir = initFolders(path)
        var counter: Long = 1
        val startDate = Date().time
        val partOfIps = mutableListOf<String>()

        val jobs = Collections.synchronizedList(mutableListOf<Job>())

        try {
            val sourceLine = Files.lines(Paths.get("$path\\ip_addresses"))
            sourceLine.forEach { ip: String ->
//                For example, my local google.com is at 64.233.187.99. That's equivalent to:
//                64*2^24 + 233*2^16 + 187*2^8 + 99 = 1089059683
                partOfIps.add(ip)
                if (counter % countOfPart == 0L) {
                    val copyList = partOfIps.toList()

                    val job = GlobalScope.launch(Dispatchers.IO) {
                        for (data in pickupIpsByType(copyList)) {
                            if (data.isNotEmpty()) {
                                addStringToFile("${testDir}/${partIpToInt(data[0])}.txt", "${data.toSet().joinToString("\n")}\n")
                            }
                        }
                    }

                    waitingForCreateNewJob(jobs, 3)

                    println("jobsList = ${jobs.count()}")

                    jobs.add(job)
                    partOfIps.clear()

                    println("Handled $counter ips")
                    println("$counter time is ${Date().time - startDate}")

                }

                counter++
            }

            println("Ждём завершения всех потоков... Time is ${Date().time - startDate}")
            try {
                jobs.joinAll()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            val tempList = pickupIpsByType(partOfIps.toList())
            if (tempList.isNotEmpty()) {
                for (data in tempList) {
                    if (data.isNotEmpty()) {
                        addStringToFile("$testDir/${partIpToInt(data[0])}.txt", "${data.toSet().joinToString("\n")}\n")
                    }
                }
            }

            jobs.clear()
            partOfIps.clear()
            sourceLine.close()

            //End of part one
            val countUniqIps = countIps(testDir)

            println("Whole time is ${Date().time - startDate}")
            println("Count uniq ips is $countUniqIps")
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {

        }
    }

    private fun addStringToFile(path: String, str: String) {
        File(path).appendText(str)
    }

    private fun pickupIpsByType(partOfIps: List<String>): MutableList<MutableList<String>> {
        val tempList = mutableListOf<MutableList<String>>()
        for (index in 0..65_536) {
            tempList.add(mutableListOf<String>())
        }
        for (ip in partOfIps) {
            tempList[partIpToInt(ip)].add(ip)
        }
        return tempList
    }

    private fun partIpToInt(ip: String): Int {
        val neededPart = ip.substringBeforeLast(".").substringBeforeLast(".").split(".").map { it.toInt() }
        return (neededPart[0] * 2f.pow(8) + neededPart[1]).toInt()
    }

    private fun getFilePathList(path: String): Set<Path> {
        return File(path).walkTopDown().filter { it.isFile }.map { it.toPath() }.toSet()
    }

    private fun initFolders(path: String): Path {
        return Files.createDirectories(Path("${path}/ip_group_files"))
    }

    private fun waitingForCreateNewJob(jobList: MutableList<Job>, maxJobCount: Int) {
        while (jobList.count() > maxJobCount) {
            try {
                if (jobList.isNotEmpty()) {
                    val iterator = jobList.iterator()
                    while (iterator.hasNext()) {
                        val myjob = iterator.next()
                        if (myjob.isCompleted) {
                            iterator.remove()
                        }
                    }
                }
            } catch (e: Exception) {
                e.message
            }
        }
    }

    private suspend fun countIps(testDir: Path): Long {
        val ips = Collections.synchronizedList(mutableListOf<Int>())
        val jobs = Collections.synchronizedList(mutableListOf<Job>())
        var uniqIpCount = 0L

        val filePathList = getFilePathList("$testDir")
        filePathList.forEach { myFile ->
            println(myFile.toString())

            val job = GlobalScope.launch(Dispatchers.IO) {
                val count = Files.lines(myFile)
                    .filter { it.isNotEmpty() }
                    .toList()
                    .toSet()
                    .count()

                ips.add(count)
            }
            jobs.add(job)
            waitingForCreateNewJob(jobs, 10)
        }
        jobs.joinAll()

        ips.forEach {
            uniqIpCount += it
        }
        return uniqIpCount
    }
}

fun main(args: Array<String>) = runBlocking {
    //Ввести свой путь до папки с фалом ip_addresses. Файл с ip адресами в этой папке должен называться ip_addresses.
    FileWork().getUniqIPs("D:\\Temp\\my_folder", 1_000_000)
    println("The end")
}